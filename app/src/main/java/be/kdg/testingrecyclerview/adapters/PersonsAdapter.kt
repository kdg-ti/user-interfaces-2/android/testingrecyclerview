package be.kdg.testingrecyclerview.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import be.kdg.testingrecyclerview.R
import be.kdg.testingrecyclerview.model.Person
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle


//De Adapter is verantwoordelijk voor
// - het ophalen van de data uit je model
// - het creëren vxan de juiste view voor die data (door de view te inflaten)
// - het plaatsen van de data op de view
//De LayoutManager roept op gepaste tijdstippen en op een efficiënte manier (hergebruik viewholders) de methodes van de Adapter aan

// De constructor van PersonAdapter heeft twee parameters die als attributen in het object bewaard worden
// De constructor roept de constructor zonder parameters van de superklasse RecyclerView.Adapter aan,
// die PersonViewHolder als generiek type heeft.
// personViewHolder is een inner klasse van PersonAdapter.
class PersonsAdapter(
    private val persons: Array<Person>,
    private val personSelectionListener: PersonSelectionListener
) : RecyclerView.Adapter<PersonsAdapter.PersonViewHolder>() {

    //De ViewHolder transporteert de View tussen de LayoutManager en de Adapter
    //Door bij create alvast de juiste velden op te halen uit de layout is hij efficiënter!

    //Definitie van de innerklasse PersonViewHolder.
    // De superklasse is ViewHolder, een inner klasse van RecyclerView
    // de constructor parameter view, wordt doorgegeven aan de constructor van de superklasse
    class PersonViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val name = view.findViewById<TextView>(R.id.name)
        val dateOfBirth = view.findViewById<TextView>(R.id.dateOfBirth)
        val photo = view.findViewById<ImageView>(R.id.photo)

        init {
            //Inspecteer de loggings: hoe vaak wordt de viewholder gecreëerd?
            Log.d("mylogs", "viewholder created!")
        }
    }

    //De LayoutManager roept deze methode aan zodra hij viewholders nodig heeft
    //Dat wil zeggen: vooral bij de opstart van de app: om de eerste ViewHolders te creëren voor de zichtbare items
    //Zodra de gebruiker gaat scrollen worden een aantal extra ViewHolders aangemaakt die alvast opgevuld kunnen worden
    //Nadien worden de ViewHolders voornamelijk hergebruikt en zal deze methode niet vaak meer aangeroepen worden.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val personView = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return PersonViewHolder(personView)
    }

    //De LayoutManager heeft deze methode nodig om te weten hoeveel items er in de lijst kunnen komen
    override fun getItemCount() = persons.size

    //Deze methode gebruikt de LayoutManager om te weten welke layout hoort bij het item op die positie.
    // Normaal gebruikt elk item dezelfde layout, maar we kunnen ook afhankelijk van het item een andere layout te kiezen
    //In dit voorbeeld maken we gebruik van 2 verschillende layouts: items op even posities worden anders getoond dan items op oneven posities
    override fun getItemViewType(position: Int): Int {
        if (position % 2 == 0) {
            return R.layout.person_in_list_left_image
        } else {
            return R.layout.person_in_list_right_image
        }
    }

    //Deze methode wordt door de LayoutManager aangeroepen zodra hij een nieuw item moet tonen.
    //De LayoutManager zal een bestaande ViewHolder hergebruiken en geeft deze mee als parameter
    //In deze methode kan je nu de juiste data ophalen en op de juiste plaats in de view zetten
    override fun onBindViewHolder(personViewHolder: PersonViewHolder, position: Int) {
        personViewHolder.name.text = persons[position].name
        personViewHolder.dateOfBirth.text = persons[position].dateOfBirth.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))
        personViewHolder.photo.setImageDrawable(ContextCompat.getDrawable(personViewHolder.itemView.context,persons[position].imageId))
        //Inspecteer de logging: hoe vaak wordt de binding gedaan in vergelijking met de creatie van de ViewHolder?
        Log.d("mylogs", "binding view to data!")
        //Via een OnClickListener vangen we het klikken op de view op en melden de selectie aan de personSelectionListener
        //Telkens we de view binden aan data moeten we ook de onClickListener aanpassen, zodat de juiste positie doorgegeven kan worden!
        personViewHolder.itemView.setOnClickListener {
            personSelectionListener.onPersonSelected(position)
        }
    }

    //Als we willen dat de activity op de hoogte gesteld wordt wanneer een Person aangeklikt wordt,
    //dan voorzien we hiervoor een specifieke interface PersonSelectionListener
    //We zorgen dat de Activity deze interface implementeert. De Activity geeft zichzelf dan mee aan de adapter als PersonSelectionListener.
    //We houden op die manier een losse koppeling naar de Activity vanuit de Adapter.
    interface PersonSelectionListener {
        fun onPersonSelected(position: Int)
    }
}