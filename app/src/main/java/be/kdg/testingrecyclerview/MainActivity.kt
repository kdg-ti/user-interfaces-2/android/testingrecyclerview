package be.kdg.testingrecyclerview

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.testingrecyclerview.adapters.PersonsAdapter
import be.kdg.testingrecyclerview.model.getPersons

//Een const is een compile time constante: vergelijkbaar met aan #DEFINE in C...!
const val PERSON_POSITION: String = "PERSON_POSITION"

//Onze MainActivity implementeert de PersonsAdapter.PersonSelectionListener,
//want hij is geïnteresseerd in de selectie van een Person
class MainActivity : AppCompatActivity(), PersonsAdapter.PersonSelectionListener {
    private lateinit var persons: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialiseViews()
    }

    private fun initialiseViews() {
        persons = findViewById<RecyclerView>(R.id.persons)
            .apply {
                //De RecyclerView heeft een LayoutManager nodig en een Adapter
                //De LayoutManager kan je zelf schrijven of je kan gebruik maken van bestaande LayoutManagers
                //zoals LineairLayoutManager, GridLayoutManager of StaggeredGridLayoutManager
                layoutManager =
									LinearLayoutManager(this@MainActivity)
                //De Adapter klasse schrijven we zelf, inspecteer de implementatie grondig!
                adapter = PersonsAdapter(getPersons(), this@MainActivity)

                //MainActivity is een 'PersonSelectionListener' en een 'Context'.
                //Om naar het MainActivity object te verwijzen kunnen we niet gewoon 'this' schrijven
                //want we zitten in een lambda waarvan 'this' gebonden is aan de 'RecyclerView'.
                //bijgevolg gebruiken we 'this@MainActivity' om de *outer* this te verkrijgen.
                //Zie ook verschil tussen 'apply' en 'also'! (herschrijf de code eens met 'also'!)
            }
    }

    //Wanneer we een Person aanklikken in de RecyclerView zal deze methode aangeroepen worden
    //We gebruiken een Intent om een andere Activity te starten en geven de positie van de Person mee als Extra
    //Check de documententatie over Intents op https://developer.android.com/guide/components/intents-filters
    override fun onPersonSelected(position: Int) {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra(PERSON_POSITION, position)
        startActivity(intent)
    }
}
