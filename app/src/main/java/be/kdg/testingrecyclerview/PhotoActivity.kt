package be.kdg.testingrecyclerview

import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import be.kdg.testingrecyclerview.model.getPersons

class PhotoActivity : AppCompatActivity() {
    private lateinit var photo: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        initialiseViews()
    }

    private fun initialiseViews() {
        photo = findViewById(R.id.photo)
        //De person positie vinden we terug in de Intent die de Activity opstartte
        val position = intent.getIntExtra(PERSON_POSITION, 0)
        Log.d("mylogs", "Position: " + position)
        photo.setImageDrawable(getDrawable(getPersons()[position].imageId))
    }
}
