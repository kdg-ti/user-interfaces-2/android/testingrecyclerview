package be.kdg.testingrecyclerview.model

import be.kdg.testingrecyclerview.R
import java.time.LocalDate

fun getPersons(): Array<Person> {
    return arrayOf(
        Person("Jos", LocalDate.of(1974, 4, 25), R.drawable.dummyman),
        Person("Jef", LocalDate.of(1975, 4, 25), R.drawable.dummyman),
        Person("Suzan", LocalDate.of(1976, 4, 25), R.drawable.dummywoman),
        Person("Mia", LocalDate.of(1974, 4, 23), R.drawable.dummyman),
        Person("Jos2", LocalDate.of(1974, 4, 25), R.drawable.dummyman),
        Person("Jef2", LocalDate.of(1975, 4, 25), R.drawable.dummyman),
        Person("Suzan3", LocalDate.of(1976, 4, 25), R.drawable.dummywoman),
        Person("Mia4", LocalDate.of(1974, 4, 23), R.drawable.dummywoman),
        Person("Jos3", LocalDate.of(1974, 4, 25), R.drawable.dummyman),
        Person("Jef12", LocalDate.of(1975, 4, 25), R.drawable.dummyman),
        Person("Suzan123", LocalDate.of(1976, 4, 25), R.drawable.dummyman),
        Person("Mia4", LocalDate.of(1974, 4, 23), R.drawable.dummywoman),
        Person("een", LocalDate.of(1974, 4, 25), R.drawable.dummyman),
        Person("twee", LocalDate.of(1975, 4, 25), R.drawable.dummywoman),
        Person("drie", LocalDate.of(1976, 4, 25), R.drawable.dummyman),
        Person("vier", LocalDate.of(1974, 4, 23), R.drawable.dummyman),
        Person("vijf", LocalDate.of(1974, 4, 25), R.drawable.dummywoman),
        Person("zes", LocalDate.of(1975, 4, 25), R.drawable.dummywoman),
        Person("zeven", LocalDate.of(1976, 4, 25), R.drawable.dummywoman),
        Person("acht", LocalDate.of(1974, 4, 23), R.drawable.dummyman),
        Person("negen", LocalDate.of(1974, 4, 25), R.drawable.dummyman),
        Person("tien", LocalDate.of(1975, 4, 25), R.drawable.dummywoman),
        Person("elf", LocalDate.of(1976, 4, 25), R.drawable.dummyman),
        Person("twaalf", LocalDate.of(1974, 4, 23), R.drawable.dummywoman)
    )
}

data class Person(
    val name: String,
    val dateOfBirth: LocalDate,
    val imageId: Int
)